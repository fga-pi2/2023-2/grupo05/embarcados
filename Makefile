SHELL := /bin/bash
IP = 15.229.75.24
USER = ubuntu
PRJ_NAME = mock
SERVER_PROJECT_PATH = /home/$(USER)/mock
PATH_MY_OS = /home/renato/Desktop/pi2/embarcados/mock_api

PROJECT_NAME := master
WAIT_TIME=5


fullclean:
	idf.py fullclean

install_esp_idf:
	sudo ./install_esp_idf.sh

build:
	idf.py build

sidf:
	. /home/$$(whoami)/esp-idf/export.sh

bidf:
	idf.py build

bidf_log:
	idf.py build &> 'logs/build_$(shell date).log'

fidf:
	idf.py flash

midf:
# para sair do monitor, é 'CTRL+]'
	idf.py monitor

fmidf:
	make fidf; make midf

bfmidf:
	make bidf; make fidf; make midf

cidf:
	idf.py fullclean

get_log:
	timeout $(WAIT_TIME)s idf.py monitor
	mv log.log monitor_log_$(shell date).log

set_env:
	export IDF_PATH=/home/renato/esp-idf
	source export.sh

build_continuous:
	while true; do echo "Building..."; make bidf_log; done

# curl_mock_longpoll:
# 	curl -X POST -H "Content-Type: application/json" -d '{"id": "1", "method": "get", "params": ["mock"]}' http://localhost:5000/longpoll

run_mock_server:
	sudo python3 mock_api/mock_dispenser_dispense.py 

curl_longpoll:
	curl -X POST -H"content-type: application/json" -d '{"test":"123", "45": 6}' http://15.229.75.24/longpoll

ssh:
	ssh -i ~/.ssh/medhelp.pem $(USER)@$(IP) 

install:
	rsync -av --exclude=".git" -e 'ssh -i ~/.ssh/medhelp.pem' ./mock_api $(USER)@$(IP):$(SERVER_PROJECT_PATH)
	@echo "app installed on target:$(SERVER_PROJECT_PATH)"

pull:
	rsync -av --exclude=".git" -e 'ssh -i ~/.ssh/medhelp.pem $(USER)@$(IP)' $(USER)@$(IP):$(SERVER_PROJECT_PATH) .. 
	@echo "app installed on target:$(SERVER_PROJECT_PATH)"


