# Embarcados - MedHelp - PI2 

O objetivo desse sistema é cumprir os requisitos para sistema embarcado do projeto Medhelp.

Entre os objetivos que este sistema cumpre, temos:
- Comunicar HTTP com backend, seguindo [este modelo de API](https://docs.google.com/document/d/1XEuo1KpfIVxLrL9BJzXGdwXXRDSFBTZwsFXRmQQAH4g).
- Realizer leitura de sensores e ativação de atuadores.
- Rodar em um microcontrolador ESP32.


## Como configurar?

Detalhes importantes:
- *ambiente completo no VSCode, mas se quiser usar so terminal basta não instalar coisas envolvendo VSCode*
- *comandos estão usando Makefile, que pode ser instalado com `sudo apt install make`*

Instruções

1. Instale [esp-idf](https://github.com/espressif/esp-idf) (com sudo) `install_esp-idf.sh` ou `make install_esp_idf`

2. (opcional) Instale a extensão [espressif](https://docs.espressif.com/projects/esp-idf/en/v4.4/esp32/get-started/vscode-setup.html) nos plugins do VSCode.

3. (opcional) Pra lintar corretamente, mude a pasta pra onde a pasta do seu esp-idf está no arquivo `.vscode/c_cpp_properties.json` linha 4 e 5:
![](imgs/Screenshot%20from%202023-07-09%2010-12-25.png)

## Como rodar? 

Detalhes importantes:
- *leia o makefile para saber os comandos que está rodando*
- *todos os comandos são executados na pasta onde este README se encontra*

Instruções

1. Conecte computador com esp32 (qualquer modelo, porta micro usb 2.0)
![](imgs/demo.jpeg)

1. Configure esp-idf no (apenas) no terminal que você está usando: `. /home/$$(whoami)/esp-idf/export.sh` ou `make sidf`

2. Compile projeto com `idf.py build` ou `make bidf`.
3. Instale projeto compilado na placa `idf.py flash` ou `make fidf`.
4. Monitore os logs da placa `idf.py monitor` ou `make midf`.


## Requisitos 

Os seguintes sensores serão usados:

- 1x buzzer
- 4x servo 'azul' SG90
- 1x servo 'preto' Tower Pro Mg995
- 1x camera
- ?x leds
- ...?

## Sistema físico

![](imgs/esp32pinout.png)

Evite usar os pinos em itálico abaixo. Não serviram para tentar ligar um led.

| Pino | Lado | Uso               |
| ---- | ---- | ----------------- |
| VP   | A    | _???_             |
| D34  | A    | _pino read only_  |
| D35  | A    | _pino read only_  |
| D32  | A    | _pino read only_  |
| D33  | A    |                   |
| D25  | A    |                   |
| D26  | A    |                   |
| D27  | A    |                   |
| D14  | A    |                   |
| D12  | A    |                   |
| D13  | A    |                   |
| D22  | B    |                   |
| TX0  | B    | _melhor não usar_ |
| RX0  | B    | _melhor não usar_ |
| D21  | B    |                   |
| D19  | B    |                   |
| D18  | B    | SERVO_1           |
| D17  | B    | SERVO_3           |
| D5   | B    |                   |
| TX2  | B    | _melhor não usar_ |
| RX2  | B    | _melhor não usar_ |
| D4   | B    |                   |
| D2   | B    |                   |
| D15  | B    |                   |


## wifi

![](imgs/router.png)

Configure essas variáveis para sua rede de wifi, de forma que a ESP irá se ligar nela quando ligar.

## troubleshooting

se tiver dando erro no flash, tenta

sudo adduser <username> dialout

sudo chmod a+rw /dev/ttyUSB0

## sistema físico

menor fileira = mais externo

fileira 1 - 28 slots
fileira 2 - 21 slots
fileira 3 - 14 slots
fileira 4 - 7 slots

ao todo, 70 slots
