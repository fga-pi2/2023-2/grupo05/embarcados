#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>
#include <Stepper.h>
#include <ESP32Servo.h>

Servo myservo[4];

// Definindo as pinagens

#define STEPPER_PIN1 13 // Pino D13 para o motor de passo
#define STEPPER_PIN2 12 // Pino D14 para o motor de passo
#define STEPPER_PIN3 14 // Pino D27 para o motor de passo
#define STEPPER_PIN4 27 // Pino D26 para o motor de passo
#define IR_MARCOZERO_PIN 15
// #define IR_SENSOR_PIN 4 // Pino D34 para o sensor IR
#define REED_SWITCH_PIN 4 // Pino D33 para o reed switch
#define LED_BUZZER_PIN 5  // Pino D2 para o LED e Buzzer

void spinRadial(int fileira, int slot);
void callLongPoll();
void moveServos(int servoNumber);
// void sensorInterrupt();
void abrir_trash();
void gaveta();

// Servo meuServo;          // Objeto Servo para controlar o servo motor
Stepper myStepper(2048, STEPPER_PIN1, STEPPER_PIN2, STEPPER_PIN3, STEPPER_PIN4); // Motor de passo

const char *ssid = "realme 8 5G";
const char *password = "6qa8mffn";

String API_URL = "http://15.229.75.24";

unsigned long lastTime = 0;
unsigned long timerDelay = 5000;

void setupWifi()
{
    WiFi.begin(ssid, password);
    Serial.println("Connecting");
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.print("Connected to WiFi network with IP Address: ");
    Serial.println(WiFi.localIP());

    Serial.println("Timer set to 5 seconds (timerDelay variable), it will take 5 seconds before publishing the first reading.");
}

void setup()
{
    Serial.begin(115200);
    setupWifi();
    // pinMode(LED_BUZZER_PIN, OUTPUT);
    pinMode(REED_SWITCH_PIN, INPUT);
    // attachInterrupt(digitalPinToInterrupt(REED_SWITCH_PIN), sensorInterrupt, LOW);

    myStepper.setSpeed(10);

    myservo[0].attach(2);
    myservo[1].attach(26);
    myservo[2].attach(32);
    myservo[3].attach(33);
    myservo[4].attach(25);
}

void loop()
{
    Serial.println("loop");
    delay(5000);

    if ((millis() - lastTime) > timerDelay)
    {
        callLongPoll();

        lastTime = millis();
    }
}

/*
void sensorInterrupt(){
digitalWrite(LED_BUZZER_PIN, LOW);
Serial.print("Gaveta Abriu! Envia pro Back");
}
*/

void callLongPoll()
{
    Serial.println("callLongPoll");
    // Check WiFi connection status
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;

        String serverPath = API_URL + "/longpoll";

        // Your Domain name with URL path or IP address with path
        http.begin(serverPath.c_str());

        // If you need Node-RED/server authentication, insert user and password below
        // http.setAuthorization("REPLACE_WITH_SERVER_USERNAME", "REPLACE_WITH_SERVER_PASSWORD");

        // Send HTTP GET request
        int httpResponseCode = http.GET();

        if (httpResponseCode > 0)
        {
            Serial.print("HTTP Response code: ");
            Serial.println(httpResponseCode);
            String payload = http.getString();

            JSONVar myObject = JSON.parse(payload);

            // JSON.typeof(jsonVar) can be used to get the type of the var
            if (JSON.typeof(myObject) == "undefined")
            {
                Serial.println("Parsing input failed!");
                return;
            }

            Serial.print("[callLongPoll] JSON object = ");
            Serial.println(myObject);

            for (int i = 0; i < myObject.length(); i++)
            {
                JSONVar fileira = myObject[i]["body"]["fileira"];
                JSONVar slot = myObject[i]["body"]["slot"];
                spinRadial(fileira, slot);
            }
        }
        else
        {
            Serial.print("Error code: ");
            Serial.println(httpResponseCode);
        }

        http.end();
    }
    else
    {
        Serial.println("WiFi Disconnected");
    }
}

void moveServos(int servoNumber)
{
    Serial.println("moveServos");
    int pos = 0;

    if (servoNumber == 0)
    {
        myservo[0].write(10);
        delay(2000);

        myservo[0].write(105);
        delay(2000);
    }
    else if (servoNumber == 1)
    {
        myservo[1].write(170);
        delay(2000);

        myservo[1].write(95);
        delay(2000);
    }
    else if (servoNumber == 2)
    {
        myservo[2].write(10);
        delay(2000);

        myservo[2].write(105);
        delay(2000);
    }
    else if (servoNumber == 3)
    {
        myservo[3].write(170);
        delay(2000);

        myservo[3].write(95);
        delay(2000);
    }
    digitalWrite(LED_BUZZER_PIN, HIGH);
}

void goBackRadial()
{
    Serial.println("go back radial()");
    while (digitalRead(IR_MARCOZERO_PIN) == HIGH)
    {
        myStepper.step(5.65);
    }

    delay(1000);
}

void abrir_trash()
{
    Serial.println("abrir trash()");
    myservo[4].write(10);
    delay(2000);

    myservo[4].write(105);
    delay(2000);
}

void gaveta()
{
    Serial.println("gaveta()");
    while (digitalRead(REED_SWITCH_PIN) == HIGH)
    {
        // digitalWrite(LED_BUZZER_PIN, HIGH);
        // delay(1000);
        // // digitalWrite(LED_BUZZER_PIN, LOW);
        // delay(750);

        Serial.println("[gaveta] entrou no while");

        // Check WiFi connection status
        if (WiFi.status() == WL_CONNECTED)
        {
            HTTPClient http;

            String serverPath = API_URL + "/longpoll";

            // Your Domain name with URL path or IP address with path
            http.begin(serverPath.c_str());

            // If you need Node-RED/server authentication, insert user and password below
            // http.setAuthorization("REPLACE_WITH_SERVER_USERNAME", "REPLACE_WITH_SERVER_PASSWORD");

            // Send HTTP GET request
            int httpResponseCode = http.GET();

            if (httpResponseCode > 0)
            {
                Serial.print("HTTP Response code: ");
                Serial.println(httpResponseCode);
                String payload = http.getString();

                JSONVar myObject = JSON.parse(payload);

                // JSON.typeof(jsonVar) can be used to get the type of the var
                if (JSON.typeof(myObject) == "undefined")
                {
                    Serial.println("Parsing input failed!");
                    return;
                }

                Serial.print("[gaveta] JSON object = ");
                Serial.println(myObject);

                for (int i = 0; i < myObject.length(); i++)
                {
                    int fileira = myObject[i]["body"]["fileira"];
                    int slot = myObject[i]["body"]["slot"];
                    bool opened = myObject[i]["body"]["opened"];
                    if (fileira == -1 && slot == -1 && opened == true)
                    {
                        abrir_trash();
                    }
                }
            }
            else
            {
                Serial.print("Error code: ");
                Serial.println(httpResponseCode);
            }

            http.end();
        }
        else
        {
            Serial.println("WiFi Disconnected");
        }
    }
    HTTPClient http;

    // Specify the target URL
    http.begin(API_URL + "/dispenser/drawer?open=true");

    // Send the GET request
    int httpResponseCode = http.GET();

    if (httpResponseCode > 0)
    {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);

        // Read the response payload
        String payload = http.getString();
        Serial.println("Response payload: " + payload);
    }
    else
    {
        Serial.print("HTTP Request failed. Error code: ");
        Serial.println(httpResponseCode);
    }
}

void spinRadial(int fileira, int slot)
{
    Serial.println("spinRadial");
    float step = 0;

    switch (fileira)
    {
    case 1:
        step = 72.6;
        break;
    case 2:
        step = 96.84;
        break;
    case 3:
        step = 145.26;
        break;
    case 4:
        step = 290.52;
        break;
    }

    myStepper.step(step * (slot - 1));
    moveServos(fileira - 1);
    goBackRadial();
    // gaveta();
}