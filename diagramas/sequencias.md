# Todas as sequências

Alguma notas:
- PRIORIDADE NA COLOCADA: Quando remédio precisar ser dispensado e caixa estiver aberta, remédio é dispensado depois

# 1. Quando um novo remédio deve ser colocado

```mermaid
sequenceDiagram
    actor U as Usuario
    participant B as Backend
    participant E as Embarcados
    loop Para cada remédio novo
        U->>B: Quero adicionar um novo remédio
        B->>E: A caixa está aberta?
        E-->>B: A caixa está fechada
        B-->>U: Usuário, abra a caixa
        E->>B: A caixa está aberta
        B->>B: Slot X está disponível
        B->>E: Novo remédio no slot X
        activate E
        E-->>B: Confirmo que o slot está vazio
        deactivate E
        B-->>U: Usuário, coloque remédio no slot X
        U->>B: Usuário confirma que colocou
    end
```

# 2. Dispensar remédio

```mermaid
sequenceDiagram
    actor U as Usuario
    participant B as Backend
    participant E as Embarcados
    B->>B: É hora do usuário X tomar 'dipirona' 
    B->>E: A caixa tá fechada?
    E-->>B: A caixa está fechada
    B->>B: Tem 'dipirona' no slot 4 fileira 2
    B->>E: Libera o slot 4 fileira 2 
    E->>E: Girei radial 2 até o slot 4
    E->>E: Abri portinhola pra fileira 2
    E->>E: Confirmei que remédio novo está na região de retirada.
    E->>E: Liguei o LED e o Buzzer
    E-->>B: Remédio do slot 4 fileira 2 liberado
```

# 2.1 Mas o usuário não tomou em 30 minutos

```mermaid
sequenceDiagram
    participant B as Backend
    participant E as Embarcados  
    Note over E,B: ...
    E->>B: Remédio slot 4 fileira 2 liberado
    Note over E,B: 30 minutos se passaram...
    E->>E: Tem remédio na região de retirada?
    E->>B: Remédios liberados não foram tomados     
    E->>E: Abre gaveta com motor de passo
    E->>E: Fecha gaveta com motor de passo
```

# 2.2 E o usuário tomou

```mermaid
sequenceDiagram
    participant B as Backend
    participant E as Embarcados  
    Note over E,B: ...
    E->>B: Remédio slot 4 fileira 2 liberado
    Note over E,B: Usuário pega remédio
    E->>E: Movimento detectado na região de tomada?
    E->>E: Camera ligada
    Note over E,B: 30 segundo se passa...
    E->>E: Camera desligada
    E->>E: Remédios liberados foram tomados?
    E->>B: Usuário tomou, aqui o vídeo: <...>
```

# 2.3 Mas o usuário botou a mão mas não pegou remédio

```mermaid
sequenceDiagram
    participant B as Backend
    participant E as Embarcados  
    Note over E,B: ...
    E->>B: Remédio slot 4 fileira 2 liberado
    Note over E,B: Usuário pega remédio
    E->>E: Movimento detectado na região de tomada?
    E->>E: Camera ligada
    Note over E,B: 30 segundo se passa...
    E->>E: Camera desligada
    E->>E: Remédios liberados foram tomados?
    E->>E: Usuário não tomou, evento ignorado
```

# 2.4 Acabou os remédios

```mermaid
sequenceDiagram
    actor U as Usuario
    participant B as Backend
    participant E as Embarcados  
    Note over E,B: ...
    E->>B: Remédio slot 4 fileira 2 liberado
    B->>B: Esse era o último remédio armazenado no radial?
    B->>U: Notifica usuário
    alt Se não reposto até o proximo horário
    B->>B: Configurações de horário reiniciadas
    end
```

# 3. Quando caixa fecha

## 3.1 Caixa fecha com slots devidamente ocupados

```mermaid
sequenceDiagram
    participant B as Backend
    participant E as Embarcados
    E->>E: Vou girar os radiais
    E->>B: Todos os slots estão devidamente ocupados.
```

## 3.2 Caixa fecha com slots erroneamente ocupados/vazios

```mermaid
sequenceDiagram
    actor U as Usuario
    participant B as Backend
    participant E as Embarcados
    E->>B: Caixa foi fechada 
    B->>E: Confirme que apenas os seguintes slots estão ocupados: [...] 
    E->>E: Vou girar os radiais
    E->>E: Verifica cada slot de cada fileira
    E->>B: O slot 3 fileira 1 está preenchido mas não deveria.
    B-)U: Remova remédio do slot 3 fileira 1.
    E->>B: O slot 6 fileira 2 está vazio mas não deveria.
    B-)U: Coloque 'dipirona' no slot 6 fileira 2.
```

