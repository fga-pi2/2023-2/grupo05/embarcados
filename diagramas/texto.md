# Sistema Embarcado

O sistema embarcado do projeto usará uma série de sensores e atuadores ligados a um microcontrolador ESP32, e este estará conectado a internet em constante comunicação com o backend pelo protocolo HTTP. 

O código do microcontrolador será em linguagem C, utilizando a biblioteca ESP-IDF como framework. Será dividido em vários módulos, cada módulo sendo uma função ou arquivo dentro da base de código.

A comunicação entre a ESP32 e os sensores e atuadores será implementada pelo uso de GPIOs integradas automaticamente ao sistema embarcado pelo código gerado na biblioteca ESP-IDF.

A instalação e uso do sistema independe do modelo de ESP32 utilizado, pois a biblioteca ESP-IDF é capaz de interoperar entre todos os modelos.

## Módulos e componentes

Os seguintes componentes do sistema embarcado foram identificados:

- Input de vídeo da camera
- Input do sensor infravermelho
- Output do servo 'azul pequeno' SG90
- Output do buzzer
- Output do led
- Output do motor de passo
- Módulo de comunicação HTTP com backend (conexão constante por long-polling)
- Módulo de controle de regras de négócio
- Módulo de abrir e fechar portinhola com servo
- Módulo de girar radial pro proximo slot com motor motor de passo
- Módulo de verificar se slot está ocupado com sensor infravermelho
- Streaming de vídeo pro backend


## Integração

<!-- 
1.   Apêndice 05 – Documentação de software
Introdução: Pequeno texto apresentará, com objetividade, o propósito da solução de software, e como vêem a integração com outras Engenharia. Também, introduzirá os seguintes tópicos. -->


O sistema embarcado estará em direto contato com as áreas de eletrônica e backend.

A área de eletrônica irá prover (montados no sistema físico) os aparelhos mencionados. Também, a área de eletrônica e embarcados irá trabalhar colaborativamente para gerar os módulos de atuadores e sensores.

A área de backend e embarcados irão se comunicar para implementar as regras de negócio. O backend envia comandos de instruções do usuário e realiza verificações. O sistema embarcados executa comandos fisicamente no aparelho pelos atuadores, e faz a leitura de sensores. Mais informações de como cada área irá interagir, e com qual propósito, podem ser encontradas abaixo. 

## Histórico de decisões

- Decidimos utilizar ESP32
- Decidimos utilizar ESP-IDF como framework de desenvolvimento
- Decidimis utilizar uma base de código de projetos anteriores já preparada
- Decidimos comunicar com backend via internet
- Decidimos comunicar com protocolo HTTP

### Diagrama de casos de uso

![](casos_de_uso.drawio.png)

### Diagramas de sequência

#### Sequência - Quando um novo remédio deve ser colocado

```mermaid
sequenceDiagram
    actor U as Usuario
    participant B as Backend
    participant E as Embarcados
    loop Para cada remédio novo
        U->>B: Quero adicionar um novo remédio
        B->>E: A caixa está aberta?
        E-->>B: A caixa está fechada
        B-->>U: Usuário, abra a caixa
        E->>B: A caixa está aberta
        B->>B: Slot X está disponível
        B->>E: Novo remédio no slot X
        activate E
        E-->>B: Confirmo que o slot está vazio
        deactivate E
        B-->>U: Usuário, coloque remédio no slot X
        U->>B: Usuário confirma que colocou
    end
```

#### Sequência - Dispensar remédio

```mermaid
sequenceDiagram
    actor U as Usuario
    participant B as Backend
    participant E as Embarcados
    B->>B: É hora do usuário X tomar 'dipirona' 
    B->>E: A caixa tá fechada?
    E-->>B: A caixa está fechada
    B->>B: Tem 'dipirona' no slot 4 fileira 2
    B->>E: Libera o slot 4 fileira 2 
    E->>E: Girei radial 2 até o slot 4
    E->>E: Abri portinhola pra fileira 2
    E->>E: Confirmei que remédio novo está na região de retirada.
    E->>E: Liguei o LED e o Buzzer
    E-->>B: Remédio do slot 4 fileira 2 liberado
```

#### Sequência - Dispensa remédio mas o usuário não tomou em 30 minutos

```mermaid
sequenceDiagram
    participant B as Backend
    participant E as Embarcados  
    Note over E,B: ...
    E->>B: Remédio slot 4 fileira 2 liberado
    Note over E,B: 30 minutos se passaram...
    E->>E: Tem remédio na região de retirada?
    E->>B: Remédios liberados não foram tomados     
    E->>E: Abre gaveta com motor de passo
    E->>E: Fecha gaveta com motor de passo
```

#### Sequência - Dispensa remédio e o usuário tomou

```mermaid
sequenceDiagram
    participant B as Backend
    participant E as Embarcados  
    Note over E,B: ...
    E->>B: Remédio slot 4 fileira 2 liberado
    Note over E,B: Usuário pega remédio
    E->>E: Movimento detectado na região de tomada?
    E->>E: Camera ligada
    Note over E,B: 30 segundo se passa...
    E->>E: Camera desligada
    E->>E: Remédios liberados foram tomados?
    E->>B: Usuário tomou, aqui o vídeo: <...>
```

#### Sequência - Dispensa remédio mas o usuário botou a mão mas não pegou remédio

```mermaid
sequenceDiagram
    participant B as Backend
    participant E as Embarcados  
    Note over E,B: ...
    E->>B: Remédio slot 4 fileira 2 liberado
    Note over E,B: Usuário pega remédio
    E->>E: Movimento detectado na região de tomada?
    E->>E: Camera ligada
    Note over E,B: 30 segundo se passa...
    E->>E: Camera desligada
    E->>E: Remédios liberados foram tomados?
    E->>E: Usuário não tomou, evento ignorado
```

#### Sequência - Acabaram os remédios

```mermaid
sequenceDiagram
    actor U as Usuario
    participant B as Backend
    participant E as Embarcados  
    Note over E,B: ...
    E->>B: Remédio slot 4 fileira 2 liberado
    B->>B: Esse era o último remédio armazenado no radial?
    B->>U: Notifica usuário
    alt Se não reposto até o proximo horário
    B->>B: Configurações de horário reiniciadas
    end
```

#### Sequência - Quando caixa fecha

#### Sequência - Quando caixa fecha com slots devidamente ocupados

```mermaid
sequenceDiagram
    participant B as Backend
    participant E as Embarcados
    E->>E: Vou girar os radiais
    E->>B: Todos os slots estão devidamente ocupados.
```

#### Sequência - Quando caixa fecha com slots erroneamente ocupados/vazios

```mermaid
sequenceDiagram
    actor U as Usuario
    participant B as Backend
    participant E as Embarcados
    E->>B: Caixa foi fechada 
    B->>E: Confirme que apenas os seguintes slots estão ocupados: [...] 
    E->>E: Vou girar os radiais
    E->>E: Verifica cada slot de cada fileira
    E->>B: O slot 3 fileira 1 está preenchido mas não deveria.
    B-)U: Remova remédio do slot 3 fileira 1.
    E->>B: O slot 6 fileira 2 está vazio mas não deveria.
    B-)U: Coloque 'dipirona' no slot 6 fileira 2.
```


## Referencias



___
___
___
___



## *!! apagar depois - sobre a entrega !!*

mostrar algo funcionando (Camera transmitindo video)

- Meta
- - Autoavaliação
- - Histórico de decisões
- - Referencias
- - Realizar a formatação no padrão ABNT
- - - CONFERIR TODAS AS REFERÊNCIAS ANTES DE COLOCAR AQUI!!!
- - - COLOCAR CITACAÇÃO > no texto <
- - - verificar dados presentes na referência, sem contradição com ref
- - - mandar referencia no BiBTex


(a) Arquitetura da Informação -> deve-se apresentar as telas, estruturas de navegação, aspectos associados ao design da solução com a utilização de ferramentas como o Figma ou similares;

(b) Inovação-> Deve-se apresentar um texto destacando-se as principais inovações a serem realizadas no projeto pela equipe de software. É desejável que haja uma pequena revisão bibliográfica para fundamentar aspectos associados a este tópico.

(c) Diagramas -> Apresentação e comentários quanto aos diagramas em geral n6ecessários para apresentar a solução proposta: diagramas de classes, diagramas de casos de uso, diagramas com protocolos de comunicação entre componentes do software, protótipos de telas, etc.
