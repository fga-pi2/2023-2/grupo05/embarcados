#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include "esp_log.h"

tdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "rtc_wdt.h"
#include "esp_task_wdt.h"
#include <rom/ets_sys.h>

// projeto
#include "wifi.h"
#include "mqtt.h"
#include "led_rgb.h"
#include "http_client.h"
#include "servo_tower_pro_sg90.h"


#define SENSOR_PIN 34 // Pino do sensor IR conectado ao D34 na ESP32
#define LED_PIN 13    // Pino do LED conectado ao D13 na ESP32

static const char *TAG = "MAIN";

void init_sensor() {
    gpio_reset_pin(SENSOR_PIN);
    gpio_set_direction(SENSOR_PIN, GPIO_MODE_INPUT);
}

void init_led() {
    gpio_reset_pin(LED_PIN);
    gpio_set_direction(LED_PIN, GPIO_MODE_OUTPUT);
}

void read_sensor() {
    int valorSensor = gpio_get_level(SENSOR_PIN);

    // Quando o valor na saída digital do Sensor for alto, ligar o LED.
    if (valorSensor == 1) {
        gpio_set_level(LED_PIN, 1);
    } else {
        gpio_set_level(LED_PIN, 0);
    }

    // Enviando o valor do sensor para o monitor serial.
    ESP_LOGI(TAG, "Valor do Sensor IR: %d", valorSensor);
}

void app_main() {
    // Inicialização do ESP-IDF
    // Inicialização dos componentes
    init_sensor();
    init_led();

    // Loop principal
    while (1) {
        read_sensor();
        vTaskDelay(1000 / portTICK_PERIOD_MS); // Aguardando 1 segundo antes da próxima leitura.
    }
}
