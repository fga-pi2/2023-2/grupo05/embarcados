#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>
#include <Stepper.h>
#include <ESP32Servo.h>

Servo myservo[4];

// Definindo as pinagens
// #define SERVO_PIN 15   // Pino D15 para o servo motor
#define STEPPER_PIN1 13 // Pino D13 para o motor de passo
#define STEPPER_PIN2 12 // Pino D14 para o motor de passo
#define STEPPER_PIN3 14  // Pino D27 para o motor de passo
#define STEPPER_PIN4 27 // Pino D26 para o motor de passo
#define IR_MARCOZERO_PIN 15 // Pino D34 para o sensor IR
#define GAVETA_PIN 14 // Pino da gaveta
#define LED_BUZZER_PIN 5 // Pino D2 para o LED e Buzzer   

// #define REED_SWITCH_PIN 33 // Pino D33 para o reed switch

void spinRadial(int fileira, int slot);
void callLongPoll();
void moveServos(int servoNumber);
void goBackRadial();

Stepper myStepper(2048, STEPPER_PIN1, STEPPER_PIN2, STEPPER_PIN3, STEPPER_PIN4); // Motor de passo

const char* ssid = "iPhone de Gustavo";
const char* password = "javascript";

String API_URL = "http://15.229.75.24/longpoll";

unsigned long lastTime = 0;
unsigned long timerDelay = 5000;

void setupWifi(){
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
 
  Serial.println("Timer set to 5 seconds (timerDelay variable), it will take 5 seconds before publishing the first reading.");
}

void setup() {
  Serial.begin(115200); 
  setupWifi();

  myStepper.setSpeed(5);

  myservo[0].attach(25);
  myservo[1].attach(33);
  myservo[2].attach(32);
  myservo[3].attach(35);
  myservo[4].attach(34);

  pinMode(GAVETA_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(GAVETA_PIN), gaveta_aberta, FALLING);
}

void loop() {
  delay(5000);

  if ((millis() - lastTime) > timerDelay) {
    callLongPoll();

    lastTime = millis();
  }
}

void gaveta_aberta(){
  Serial.println("GAVETA ABRIU CARAI!!!");
  digitalWrite(LED_BUZZER_PIN,LOW);

   if(WiFi.status()== WL_CONNECTED){
    HTTPClient http;

    String serverPath = API_URL + "/dispenser/drawer?opened=true";
    
    http.begin(serverPath.c_str());
    
    int httpResponseCode = http.GET();
    
    if (httpResponseCode>0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      String payload = http.getString();

      JSONVar myObject = JSON.parse(payload);

    if (JSON.typeof(myObject) == "undefined") {
      Serial.println("Parsing input failed!");
      return;
    }
  
    Serial.print("JSON object = ");
    Serial.println(myObject);
  
    for (int i = 0; i < myObject.length(); i++) {
      // JSONVar fileira = myObject[i]["body"]["fileira"];
      // JSONVar slot = myObject[i]["body"]["slot"];
      // spinRadial(fileira, slot);
      // JSONVar trash = myObject[i]["body"]["trash"];
    }

    } else {
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    }

    http.end();
  }
  else {
    Serial.println("WiFi Disconnected");
  }

  delay(100);
}

void callLongPoll(){
  if(WiFi.status()== WL_CONNECTED){
    HTTPClient http;

    String serverPath = API_URL + "";
    
    http.begin(serverPath.c_str());
    
    int httpResponseCode = http.GET();
    
    if (httpResponseCode>0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      String payload = http.getString();

      JSONVar myObject = JSON.parse(payload);

    if (JSON.typeof(myObject) == "undefined") {
      Serial.println("Parsing input failed!");
      return;
    }
  
    Serial.print("JSON object = ");
    Serial.println(myObject);
  
    for (int i = 0; i < myObject.length(); i++) {
      JSONVar fileira = myObject[i]["body"]["fileira"];
      JSONVar slot = myObject[i]["body"]["slot"];
      spinRadial(fileira, slot);

      JSONVar trash = myObject[i]["body"]["trash"];

      // if(fileira == 4){
      //   openTrash();
      // }
    }

    }else {
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    }

    http.end();
  }
  else {
    Serial.println("WiFi Disconnected");
  }
}

void moveServos(int servoNumber){
  int pos = 0;

  for (pos = 0; pos <= 180; pos += 1) { 
    // in steps of 1 degree
    myservo[servoNumber-1].write(pos);             
    delay(5);                       
  }
  for (pos = 180; pos >= 0; pos -= 1) { 
    myservo[servoNumber-1].write(pos);             
    delay(5);                      
  }


}

void spinRadial(int fileira, int slot){
  float step=0;

  switch(fileira) {
  case 1:
    step = 72.6;
    break;
  case 2:
    step = 96.84;
    break;
  case 3:
    step = 145.26;
    break;
  case 4:
    step = 290.52;
    break;
  }

  myStepper.step(step*(slot-1));
  moveServos(fileira);
  goBackRadial();
  digitalWrite(LED_BUZZER_PIN,HIGH);
}

void goBackRadial(){
  while(digitalRead(IR_MARCOZERO_PIN) == HIGH){
    myStepper.step(5.65);
  }

  delay(10000);
}

void openTrash(){
  int pos = 0;

   for (pos = 0; pos <= 60; pos += 1) { 
    // in steps of 1 degree
    myservo[1].write(pos);             
    delay(5);                       
  }
  for (pos = 60; pos >= 0; pos -= 1) { 
    myservo[1].write(pos);             
    delay(5);                      
  }
}