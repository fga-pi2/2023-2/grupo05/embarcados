#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "rtc_wdt.h"
#include "esp_task_wdt.h"
#include <rom/ets_sys.h>

#include "driver/gpio.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"

#include "botao.h"

static const char *TAG = "botao";

void init_botao(int pin)
{
    ESP_LOGI(TAG, "init_botao pin=%d", pin);
    gpio_reset_pin(pin);
    gpio_set_direction(pin, GPIO_MODE_INPUT);
}

int is_botao_apertado(int pin)
{
    int detecao = gpio_get_level(pin) != 0;
    ESP_LOGI(TAG, "init_botao pin=%d detecao=%d", pin, detecao);
    return detecao;
}
