#ifndef BOTAO_H
#define BOTAO_H

void init_botao(int pin);

int is_botao_apertado(int pin);

#endif