#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "rtc_wdt.h"
#include "esp_task_wdt.h"
#include <rom/ets_sys.h>

#include "driver/gpio.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"

#include "buzzer.h"

static const char *TAG = "buzzer";

void init_buzzer(int pin)
{
    ESP_LOGI(TAG, "init_buzzer pin=%d", pin);
    gpio_reset_pin(pin);
    gpio_set_direction(pin, GPIO_MODE_INPUT);
}

void buzzer_task(void *pvParameter)
{
    int pin = (int)pvParameter;
    gpio_set_direction(pin, GPIO_MODE_OUTPUT);
    gpio_set_level(pin, 1);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    gpio_set_level(pin, 0);
    vTaskDelete(NULL);
}

void set_buzzer(int pin)
{
    ESP_LOGI(TAG, "set_buzzer pin=%d", pin);
    xTaskCreate(&buzzer_task, "buzzer_task", 2048, (void *)pin, 5, NULL);
}
