#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "esp_sntp.h"
#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "rtc_wdt.h"
#include "esp_task_wdt.h"
#include <rom/ets_sys.h>

// projeto
#include "endpoints.h"
#include "controle.h"
#include "botao.h"
#include "servo_tower_pro_sg90.h"
#include "timestamp.h"
#include "motor_de_passo.h"

// [TODO] Temos 8 servos (4 portinhola). ta faltando servo
// escolha qualquer porta
#define PIN_SERVO_3 17 // portinhola 1
#define PIN_SERVO_1 18 // portinhola 2
#define PIN_SERVO_2 20 // portinhola 3
#define PIN_SERVO_4 21 // portinhola 4
#define PIN_SERVO_5 25 // escotilha
#define PIN_STEP_MOTOR 1
#define PIN_IR_SENSOR_3 10
#define PIN_IR_SENSOR_4 11
#define PIN_IR_SENSOR_2 12
#define PIN_IR_SENSOR_1 13
#define PIN_BOTAO_1 22
// fazemos tudo no mesmo pino
#define PIN_BUZZER 14
#define PIN_LED PIN_BUZZER

#define N_FILEIRAS 4
#define N_SLOTS_FILEIRA_1 28
#define N_SLOTS_FILEIRA_2 21
#define N_SLOTS_FILEIRA_3 14
#define N_SLOTS_FILEIRA_4 7
#define N_SLOTS_TOTAL (N_SLOTS_FILEIRA_1 + N_SLOTS_FILEIRA_2 + N_SLOTS_FILEIRA_3 + N_SLOTS_FILEIRA_4)

#define LONG_POLL_PERIOD 3000

#define SLOT_OPEN_TIME 1000

struct AngulosSlot
{
    int fileira;
    int slot;
    float angulo;
};
typedef struct AngulosSlot AngulosSlot;

// Pauta reunião de hj:
// - Verificar o que foi desenvolvido e o que falta ser.
// - Discutir como será a comunicação entre as chamadas de API e os componentes.
// - Decidir como será o rotação dos radiais e como armazenamento da posição relativa dos radiais.
// -  Discutir como será solucionado o problema com as portas e quais serão usadas para integração.
// - Discutir os problemas e dificuldades encontra e como solucionar.

// - tampa grande problema
// - todos os radiais rodam juntos, só um motor de passo girando
// - esp comum não tem todas as portas que precisamos
// - tem 4 portinholas

void init_camera(int id)
{
}
void init_buzzer(int id)
{
}
void init_led(int id)
{
}
void init_IR_sensor(int id)
{
}

void tirar_foto(int pin);              // camera
void ligar_buzzer_n_segundos(int pin); // buzzer
void ligar_led_n_segundos(int pin);    // led
// void girar_proximo_slot_n_graus(int pin); // motor de passo
int esse_slot_esta_ocupado(int fileira);

void piscar_led_por_n_segundos(int segundos);
void ligar_buzzer_por_n_segundos(int segundos);
void ligar_motor_de_passo();
void gravar_notificacao_de_remedio_nao_tomado_pro_backend();
int gaveta_estado();

// abrir=1 é abrir, abrir=0 é fechar
void abrir_fechar_portinhola(AppEstado *estado, int n_portinhola)
{
    int pin;
    switch (n_portinhola)
    {
    case 1:
        pin = estado->pin1_servo;
        break;
    case 2:
        pin = estado->pin2_servo;
        break;
    case 3:
        pin = estado->pin3_servo;
        break;
    case 4:
        pin = estado->pin4_servo;
        break;
    case 5:
        pin = estado->pin5_servo;
        break;
    default:
        // ESP_LOGE("abrir fechar portinhola", "portinhola %d nao existe!!", n_portinhola);
        return;
    }

    ESP_LOGE("abrir_fechar_portinhola", "pin=%d", pin);

    definir_direcao_servo(pin, 45);

    vTaskDelay(700 / portTICK_PERIOD_MS);

    definir_direcao_servo(pin, 0);
}

int is_tampa_aberta()
{
    // [TODO] pode ser monitor de energia
    return is_botao_apertado(PIN_BOTAO_1);
}

AppEstado init_estado()
{
    AppEstado estado;

    // estado.pin1_infarvemelho = 27;
    // estado.pin2_infarvemelho = 26;
    // estado.pin3_infarvemelho = 25;
    // estado.pin4_infarvemelho = 33;
    // estado.pin5_infarvemelho = 33;

    estado.pin1_servo = 22;
    estado.pin2_servo = 23;
    estado.pin3_servo = PIN_SERVO_4;
    estado.pin4_servo = PIN_SERVO_5;
    estado.pin5_servo = PIN_SERVO_5;

    // estado.pin_led_buzzer = 14;

    estado.pin1_motor_de_passo = 13;
    estado.pin2_motor_de_passo = 12;
    estado.pin3_motor_de_passo = 14;
    estado.pin4_motor_de_passo = 27;

    estado.is_remedio_na_gaveta = 0;
    estado.is_tampa_aberta = 0;

    estado.angulo = 0;

    estado.portinhola_1_aberta = 0;
    estado.portinhola_2_aberta = 0;
    estado.portinhola_3_aberta = 0;
    estado.portinhola_4_aberta = 0;

    estado.portinhola_1_fechar_timestamp = 0;
    estado.portinhola_2_fechar_timestamp = 0;
    estado.portinhola_3_fechar_timestamp = 0;
    estado.portinhola_4_fechar_timestamp = 0;

    return estado;
}

void init_gpios(AppEstado *estado)
{
    // servo (motorzinho de angulo)
    // init_servo(estado->pin1_servo);
    // init_servo(estado->pin2_servo);
    // init_servo(estado->pin3_servo);
    // init_servo(estado->pin4_servo);
    // init_servo(estado->pin5_servo);
    // ligar_servo(estado->pin1_servo);
    // ligar_servo(pin_servo_2);
    // ligar_servo(pin_servo_3);
    // ligar_servo(pin_servo_4);
    // ligar_servo(pin_servo_5);

    // init_buzzer(estado->pin_led_buzzer);
    // init_led(estado->pin_led_buzzer);

    // motor de passo (pra ação linear de abrir e fechar gaveta)
    // [TODO] CONFIG DA MOTOR DE PASSO
    init_motor_gpio(estado->pin1_motor_de_passo, estado->pin2_motor_de_passo, estado->pin3_motor_de_passo, estado->pin4_motor_de_passo);

    // sensor de infravermelho
    // [TODO] CONFIG DO INFRAVERMELHO
    // init_IR_sensor(estado->pin1_infravermelho);
    // init_IR_sensor(estado->pin2_infravermelho);
    // init_IR_sensor(estado->pin3_infravermelho);
    // init_IR_sensor(estado->pin4_infravermelho);
    // init_IR_sensor(estado->pin5_infravermelho);

    // botao (tampa da caixa aberta ou fechada)
    // init_botao(PIN_BOTAO_1);
}

float get_angulo(int slot, int n_slots)
{
    return (360.0 / (float)n_slots) * ((float)(slot - 1) + 0.5);
}

int compareAngles(const void *a, const void *b)
{
    AngulosSlot *studentA = (AngulosSlot *)a;
    AngulosSlot *studentB = (AngulosSlot *)b;
    return (studentB->angulo - studentA->angulo < 0) - (studentB->angulo - studentA->angulo > 0);
}

// retorna novo angulo do motor após girada
// READ ONLY!!!!!!!
float get_angulo_rotacao_motor_de_passo(AppEstado *estado, float angulo_alvo)
{
    float angulo_old = estado->angulo;

    float girada = angulo_alvo - angulo_old;
    if (girada < 0)
    {
        ESP_LOGE("motor", "vai precisar dar a volta");
        girada = 360.0 - girada;
    }
    while (girada > 360)
        girada -= 360.0;

    ESP_LOGE("get_angulo_rotacao_motor_de_passo", "ultimo angulo = %f", angulo_old);
    ESP_LOGE("get_angulo_rotacao_motor_de_passo", "devemos ir pra angulo %f", angulo_alvo);
    ESP_LOGE("get_angulo_rotacao_motor_de_passo", "angulo pro proixmo slot = %f", girada);
    angulo_old += girada;
    while (angulo_old > 360.0)
    {
        angulo_old -= 360.0;
    }
    ESP_LOGE("get_angulo_rotacao_motor_de_passo", "novo angulo = %f", angulo_old);

    return girada;
}

int sf2slotId(int slot, int fileira)
{
    int count_slots[] = {28, 21, 14, 7};
    int id = 0;
    for (int i = 0; i < fileira - 1; i++)
    {
        id += count_slots[i];
    }
    id += slot;
    return id;
}

void slotId2sf(int slotId, int *pslot, int *pfileira)
{
    int count_slots[] = {28, 21, 14, 7};
    for (int i = 0; i < 4; i++)
    {
        if (slotId - count_slots[i] > 0)
        {
            slotId -= count_slots[i - 1];
            continue;
        }
        *pslot = slotId;
        *pfileira = i + 1;
        return;
    }
}

int *get_slots_ocupados(AppEstado *estado)
{
    int angs[] = {360.0 / 28.0, 360.0 / 21.0, 360.0 / 14.0, 360.0 / 7.0};
    int count_slots[] = {28, 21, 14, 7};
    int *slots_ocupados = malloc(sizeof(int) * N_SLOTS_TOTAL);

    AngulosSlot slot_angulo[N_SLOTS_TOTAL];

    int cnt = 0;
    for (int j = 0; j < 4; j++)
    {
        for (int i = 0; i < count_slots[j]; i++)
        {
            slot_angulo[cnt].fileira = j + 1;
            slot_angulo[cnt].slot = i + 1;
            slot_angulo[cnt].angulo = get_angulo(i + 1, count_slots[j]);
            ESP_LOGE("get_slots_ocupados", "fileira=%d slot=%d angulo_do_slot=%f", slot_angulo[cnt].fileira, slot_angulo[cnt].slot, slot_angulo[cnt].angulo);

            cnt++;
        }
    }

    qsort(slot_angulo, N_SLOTS_TOTAL, sizeof(AngulosSlot), compareAngles);

    float last_angulo = estado->angulo;

    for (int i = 0; i < N_SLOTS_TOTAL; i++)
    {
        ESP_LOGE("get_slots_ocupados", "------------------------------------------------------");
        ESP_LOGE("get_slots_ocupados", "fileira=%d slot=%d angulo=%f", slot_angulo[i].fileira, slot_angulo[i].slot, slot_angulo[i].angulo);

        int fileira = slot_angulo[i].fileira;
        int slot = slot_angulo[i].slot;
        float angulo_new = slot_angulo[i].angulo;

        ESP_LOGE("get_slots_ocupados", "VAMOS PRA FILEIRA=%d SLOT=%d", fileira, slot);

        float girada = get_angulo_rotacao_motor_de_passo(estado, angulo_new);

        // [TODO]
        ESP_LOGE("get_slots_ocupados", "girando motor de passo %.2f graus", girada);
        rotate_motor(estado->pin1_motor_de_passo, girada);
        estado->angulo += girada;

        // [TODO] infravermelho pra ver ser slot ta ocupado ou n
        int ocupado = 1;
        slots_ocupados[sf2slotId(slot, fileira)] = ocupado;

        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    return slots_ocupados;
}

void liberar_slot(AppEstado *estado, int fileira, int slot)
{
    int count_slots[] = {28, 21, 14, 7};

    float angulo_slot = get_angulo(slot, count_slots[fileira - 1]);

    float angulo_fileira = estado->angulo;

    ESP_LOGE("liberar_slot", "fileira=%d slot=%d angulo_do_slot=%.2f angulo_da_fileira=%.2f", fileira, slot, angulo_slot, angulo_fileira);

    float girada = get_angulo_rotacao_motor_de_passo(estado, angulo_slot);

    rotate_motor(estado->pin1_motor_de_passo, girada);
    estado->angulo += girada;

    abrir_fechar_portinhola(estado, fileira);
}

void demo_motor_de_passo(AppEstado *estado)
{
    ESP_LOGE("motor", "inicia pino servo 1");
    // ligar_servo(pin_servo_1);

    while (1)
    {
        ESP_LOGE("motor", "rodando motor");

        rotate_motor(estado->pin1_motor_de_passo, 20);
    }
    // while (1)
    // {
    //     ESP_LOGE("motor", "bota servo 0 graus ");
    //     definir_direcao_servo(pin_servo_1, 0);
    //     vTaskDelay(1000 / portTICK_PERIOD_MS);

    //     ESP_LOGE("motor", "bota servo 40 graus ");
    //     definir_direcao_servo(pin_servo_1, -45);
    //     vTaskDelay(1000 / portTICK_PERIOD_MS);
    // }
}

void multiplex_mensagem_backend(ReqInfo *res, AppEstado *estado)
{
    char msg[10000];
    sprintf(msg, "%s %s", res->method, res->endpoint);

    ESP_LOGI("multiplex_mensagem_backend", "multiplexing %s", msg);

    // o backend enviou mensagem long poll pra dispensar remédio
    if (strcmp(msg, "POST /dispenser/dispense") == 0)
    {

        // SAMPLE REQ:
        // {
        //     "fileira": 2

        //     "slot": 3
        // }

        int slot;
        int fileira;
        cJSON *json = res->args;

        slot = cJSON_GetObjectItem(json, "slot")->valueint;
        fileira = cJSON_GetObjectItem(json, "fileira")->valueint;

        ESP_LOGI("LONG POLL", "liberar remédio fileira=%d slot=%d", fileira, slot);

        liberar_slot(estado, fileira, slot);

        // [TODO]
        // endpoint_dispenser_dispense("POST /dispe", fileira, slot);

        return;
    }

    // // o backend quer saber se a tampa está aberta ou fechada
    // if (strcmp(msg, "POST /dispenser/lid") == 0)
    // {
    //     int tampa_aberta = is_tampa_aberta();
    //     endpoint_dispenser_lid(tampa_aberta);
    //     return;
    // }

    // o embarcados avisa o backend que houve uma discrepancia nos remédios (tem algum slot onde não deveria estar
    if (strcmp(msg, "POST /dispenser/slots/verify") == 0)
    {
        int *res = get_slots_ocupados(estado);
        // [TODO] enviamos aqui a leitura de cada slot de cada fileira
        // [0,1,1,0,...]
        // {
        //  "fileira1": [1,2,],
        //...
        // }

        int *fileira1 = malloc(sizeof(int) * N_SLOTS_FILEIRA_1);
        int *fileira2 = malloc(sizeof(int) * N_SLOTS_FILEIRA_2);
        int *fileira3 = malloc(sizeof(int) * N_SLOTS_FILEIRA_3);
        int *fileira4 = malloc(sizeof(int) * N_SLOTS_FILEIRA_4);
        int i1 = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;

        for (int i = 0; i < N_SLOTS_TOTAL; i++)
        {
            if (i <= 28)
                fileira1[i1++] = res[i];
            else if (i <= 49)
                fileira2[i2++] = res[i];
            else if (i <= 63)
                fileira3[i3++] = res[i];
            else
                fileira4[i4++] = res[i];
        }

        // endpoint_dispenser_slots_verify(
        //     res,
        //     fileira1,
        //     fileira2,
        //     fileira3,
        //     fileira4);

        free(res);
        free(fileira1);
        free(fileira2);
        free(fileira3);
        free(fileira4);
        return;
    }

    // // o embarcados avisa o backend que houve uma discrepancia nos remédios
    // if (strcmp(msg, "POST /dispenser/alerts/list") == 0)
    // {
    //     // [TODO]
    //     return;
    // }

    // o embarcados avisa o backend que remédio foi tomado
    if (strcmp(msg, "POST /dispenser/medication/taken") == 0)
    {
        // [TODO] vai ficar pro final, com o infravermelho identificando que o medicamento foi pegado(rota post da esp para o backend, não logpoll)
        // int slot;
        // int64_t time = get_timestamp();
        // endpoint_dispenser_medication_taken(slot, time);
        return;
    }

    // o embarcados avisa o backend que remédio NÃO foi tomado
    if (strcmp(msg, "POST /dispenser/medication/not-taken") == 0)
    {
        // [TODO] recebeu do longpoll que não foi tomado
        // int slot;
        // int64_t time = get_timestamp();
        // endpoint_dispenser_medication_not_taken(slot, time);
        return;
    }

    ESP_LOGE("multiplex_mensagem_backend", "failed to multiplex %s!!", msg);
}

void init_sistema(AppEstado *estado)
{
    // abrir_fechar_portinhola(estado, 1, /*fechar*/ 0);
    // abrir_fechar_portinhola(estado, 2, /*fechar*/ 0);
    // abrir_fechar_portinhola(estado, 3, /*fechar*/ 0);
    // abrir_fechar_portinhola(estado, 4, /*fechar*/ 0);
}

void controlar_sistema_embarcado()
{
    AppEstado estado = init_estado();
    init_gpios(&estado);
    // get_slots_ocupados(&estado);
    demo_motor_de_passo(&estado);
    return;

    // init_sistema(&estado);

    while (1)
    {
        // manutencao_sistema(&estado);

        int n_reqs;
        ReqInfo *reqs = make_long_poll_call(&n_reqs);

        ESP_LOGI("", "make_long_poll_call returned %d calls", n_reqs);

        for (int i = 0; i < n_reqs; i++)
        {
            ESP_LOGI("controlar_sistema_embarcado", "======= %d ->", i);

            ReqInfo req = reqs[i];

            ESP_LOGI("controlar_sistema_embarcado", "  endpoint: %s", req.endpoint);

            ESP_LOGI("controlar_sistema_embarcado", "  method: %s", req.method);

            ESP_LOGI("controlar_sistema_embarcado", "  timestamp: %s", req.timestamp);

            ESP_LOGI("controlar_sistema_embarcado", "  body: %s", cJSON_PrintUnformatted(req.args));

            // multiplex_mudanca_de_estado(res, &estado);

            multiplex_mensagem_backend(&reqs[i], &estado);
        }

        vTaskDelay(LONG_POLL_PERIOD / portTICK_PERIOD_MS);
    }
}

// void girar_fileira_n_graus(AppEstado *estado, int fileira, float angulo)
// {
//     // int pin, n_slots;
//     // float angulo_slot;
//     // float *ang_ponteiro;

//     // get_info_fileira(fileira, estado, &pin, &angulo_slot, &ang_ponteiro, &n_slots);

//     // // [TODO] girar servo pra algum angulo

//     // *ang_ponteiro += angulo;
//     // while (*ang_ponteiro > 360)
//     // {
//     //     *ang_ponteiro -= 360;
//     // }
// }

// void aplicar_regras_de_negocio()
// {
//     // // aqui temos o exemplo de uma regra de negócio que teremos que implementar

//     // if (remedio_in_base && remedio_liberado_ha_30_minutos)
//     // {
//     //     ligar_motor_de_passo();

//     //     gravar_notificacao_de_remedio_nao_tomado_pro_backend();
//     // }
// }

// void manutencao_sistema(AppEstado *estado)
// {
//     // [TODO] quais leituras fazemos?
//     // estado->is_tampa_aberta = is_tampa_aberta();

//     ESP_LOGE("manutencao_sistema", "estado->portinhola_1_aberta = %d | %lld | %lld", estado->portinhola_1_aberta, get_timestamp(), estado->portinhola_1_fechar_timestamp);
//     //  fecha portinholas abertas em 5 segundos
//     if (estado->portinhola_1_aberta && (get_timestamp() - estado->portinhola_1_fechar_timestamp) > 0)
//     {
//         abrir_fechar_portinhola(estado, 1, /*fechar*/ 0);
//     }

//     // if (estado->portinhola_2_aberta && (get_timestamp() - estado->portinhola_2_fechar_timestamp) > 0)
//     // {
//     //     abrir_fechar_portinhola(estado, 2, /*fechar*/ 0);
//     // }

//     // if (estado->portinhola_3_aberta && (get_timestamp() - estado->portinhola_3_fechar_timestamp) > 0)
//     // {
//     //     abrir_fechar_portinhola(estado, 3, /*fechar*/ 0);
//     // }

//     // if (estado->portinhola_4_aberta && (get_timestamp() - estado->portinhola_4_fechar_timestamp) > 0)
//     // {
//     //     abrir_fechar_portinhola(estado, 4, /*fechar*/ 0);
//     // }
// }