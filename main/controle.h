#ifndef CONTROLE_H
#define CONTROLE_H

#include <inttypes.h>

struct AppEstado
{
    int is_remedio_na_gaveta;
    int is_tampa_aberta;

    int pin_led_buzzer;

    int pin1_infravermelho;
    int pin2_infravermelho;
    int pin3_infravermelho;
    int pin4_infravermelho;
    int pin5_infravermelho;

    int pin1_motor_de_passo;
    int pin2_motor_de_passo;
    int pin3_motor_de_passo;
    int pin4_motor_de_passo;
    float angulo;

    int pin1_servo;
    int pin2_servo;
    int pin3_servo;
    int pin4_servo;
    int pin5_servo;

    int portinhola_1_aberta;
    int portinhola_2_aberta;
    int portinhola_3_aberta;
    int portinhola_4_aberta;
    int portinhola_5_aberta;

    int64_t portinhola_1_fechar_timestamp;
    int64_t portinhola_2_fechar_timestamp;
    int64_t portinhola_3_fechar_timestamp;
    int64_t portinhola_4_fechar_timestamp;
    int64_t portinhola_5_fechar_timestamp;
};
typedef struct AppEstado AppEstado;

void controlar_sistema_embarcado();

#endif
