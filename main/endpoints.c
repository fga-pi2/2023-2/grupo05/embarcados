#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <stdio.h>

#include "cJSON.h"
#include "esp_sntp.h"
#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "rtc_wdt.h"
#include "esp_task_wdt.h"
#include <rom/ets_sys.h>

// projeto
#include "endpoints.h"
#include "wifi.h"
#include "http_client.h"

#define URL_MOCK "http://192.168.96.193:80"
#define API_PROD "http://15.229.75.24"
#define API_HOMOLOG "http://15.229.75.24"

#define DEV 0
#define HOMOLOG 1
#define API_ENDPOINT DEV ? URL_MOCK : HOMOLOG ? API_HOMOLOG \
                                              : API_PROD

#define RES_SIZE 10000
char longpoll_data[RES_SIZE];

char res_ignore[1000];

void get_req(char *endpoint, char *res)
{
    char url[1000];
    sprintf(url, "%s%s", API_ENDPOINT, endpoint);
    ESP_LOGI("get_req", "GET %s", url);
    http_get_request(url, res, RES_SIZE);
    ESP_LOGI("post_req", "--- GET RETURNED");
}

void post_req(char *endpoint, char *data, char *res)
{
    char url[1000];
    sprintf(url, "%s%s", API_ENDPOINT, endpoint);
    ESP_LOGI("post_req", "POST %s - %s", url, data);
    http_post_request(url, data, res, RES_SIZE);
    ESP_LOGI("post_req", "--- POST RETURNED");
}

ReqInfo *make_long_poll_call(int *n_res)
{
    ESP_LOGI("long_poll", "making long poll req");

    char data[] = "{\"testdata\": 1}";
    char res[RES_SIZE];
    post_req("/longpoll", data, res);

    // SAMPLE RESPONSE:
    // [
    //   {
    //     "body": {
    //       "actual": "empty",
    //       "expected": "occupied",
    //       "slotId": 1
    //     },
    //     "endpoint": "/dispenser/alerts/discrepancy",
    //     "method": "POST",
    //     "timestamp": "2023-12-04@13-38-56.040376"
    //   }
    // ]

    cJSON *json = cJSON_Parse(res);

    int n_items = cJSON_GetArraySize(json);
    *n_res = n_items;
    ReqInfo *reqs = malloc(sizeof(ReqInfo) * n_items);
    for (int i = 0; i < n_items; i++)
    {
        cJSON *call = cJSON_GetArrayItem(json, i);

        char *endpoint = cJSON_GetObjectItem(call, "endpoint")->valuestring;
        cJSON *args = cJSON_GetObjectItem(call, "body");
        char *method = cJSON_GetObjectItem(call, "method")->valuestring;
        char *timestamp = cJSON_GetObjectItem(call, "timestamp")->valuestring;

        ReqInfo req_info = {
            .endpoint = endpoint,
            .method = method,
            .timestamp = timestamp,
            .args = args};

        reqs[i] = req_info;

        ESP_LOGI("LONG POLL CALL", "[%s] %s %s: %s", timestamp, method, endpoint, cJSON_PrintUnformatted(args));
    }

    return reqs;

    // // show 'slotId' integer, not case sensitive
    // if (cJSON_HasObjectItem(json, "result"))
    // {
    //     json = cJSON_GetObjectItem(json, "result");
    //     // get first item in list of result
    //     json = cJSON_GetArrayItem(json, 0);
    //     if (cJSON_HasObjectItem(json, "slotId"))
    //     {
    //         ESP_LOGI("LONG POLL", "HAS SLOTID");
    //         cJSON *slotId = cJSON_GetObjectItem(json, "slotId");
    //         ESP_LOGI("LONG POLL", "slotId= %d", slotId->valueint);
    //     }
    //     else
    //     {
    //         ESP_LOGE("LONG POLL", "RESPONSE JSON HAS NO SLOTID");
    //     }
    // }
    // else
    // {
    //     ESP_LOGE("LONG POLL", "RESPONSE JSON HAS NO result");
    // }
}

// Endpoint: GET /dispenser/dispense
// Description: Instruct the embedded system to dispense a pill from a specific slot.
// Response Body:
// { “result”:
// [ array of {
// slotId: Integer
// }]
// }
// slotId: Integer
// Response: Success or error message.
void endpoint_dispenser_dispense(int slotId)
{
    // make_req("/dispenser/dispense?slotId=3", data);
}

void endpoint_dispenser_lid(int estado)
{
    // Endpoint: POST /dispenser/lid
    // Description: Update the state of the lid (open or closed).
    // Request Body:
    // state: String ("open" or "closed")
    // Response: Success or error message.
    // Note: When the state is "closed", the backend triggers a process to check the slots.
    // Slot Checking by Embedded System
    // Initiate Slot Check
    // Description: When the lid is reported closed, the backend sends a message to the embedded system to initiate a slot check.
    // Process: The backend sequentially requests the embedded system to check each slot.
    // Communication: This could be a series of internal commands rather than RESTful API endpoints, depending on your system's architecture.

    char open[] = "{\"state\": \"open\"}";
    char close[] = "{\"state\": \"closed\"}";
    char endpoint[] = "/dispenser/lid/";
    char url[1000];
    char res[1000];

    ESP_LOGI("DISPENSER LID", "CHECK DISPENSER LID STATE");

    sprintf(url, "%s%s", API_ENDPOINT, endpoint);

    if (estado == 1)
    {
        post_req(url, open, res);
        ESP_LOGI("DISPENSER LID OPEN", "------POSTED");
    }
    else
    {
        post_req(url, close, res);
        ESP_LOGI("DISPENSER LID CLOSED", "------POSTED");
    }

    // reposta ignorada
}

void endpoint_dispenser_slots_verify(int *fileira1, int *fileira2, int *fileira3, int *fileira4)
{
    // Endpoint:
    // Reporting Slot Status
    // Report Slot Status
    // Endpoint: POST /dispenser/slots/{slotId}
    // Description: The embedded system reports the status of a slot.
    // Request Body:
    // status: String ("occupied" or "empty")
    // Response: Success or error message.
    // Handling Discrepancies
    // Discrepancy Alert
    // char ocup[] = "occupied";
    // char empt[] = "empty";
    // char *status_str = status == 1 ? ocup : empt;

    // char endpoint[150];
    // sprintf(endpoint, "/dispenser/lid/");

    // char data[300];
    // sprintf(data, "{\"status\": %s, \"fileira\": %d, \"slot\": %d}", status_str, fileira, slot);

    // ESP_LOGI("dispenser slot", "check dispenser slot STATE");

    // post_req(endpoint, data, res_ignore);
    // ESP_LOGI("dispenser slot open", "------POSTED");
}

void endpoint_dispenser_alerts_discrepancy()
{
    // Endpoint: POST /dispenser/alerts/discrepancy
    // Description: Trigger an alert if there's a discrepancy in a slot's status.
    // Request Body:
    // slotId: Integer
    // expected: String ("occupied" or "empty")
    // actual: String ("occupied" or "empty")
    // Response: Success or error message.
    // Note: This endpoint could be used to notify the backend (and potentially the user via the frontend) of any discrepancies.
    // List Alerts
}

void endpoint_dispenser_alerts_list()
{
    // Endpoint: POST /dispenser/alerts/list
    // Description: Trigger an alert if there's a discrepancy in a slot's status.
    // Request Body:
    // TODO
    // Response: Success or error message.
    // Note: This endpoint could be used to notify the backend (and potentially the user via the frontend) of any discrepancies.
    // Register medication Intake
    // medication Taken
}

void endpoint_dispenser_medication_taken(int fileira, int slot, int64_t dateTime)
{
    // Endpoint: POST /dispenser/medication/taken
    // Description: The embedded system informs the backend when a pill is taken from the box.
    // Request Body:
    // slotId: Integer
    // timeTaken: DateTime
    // video: VIDEO_FORMAT
    // Response: Success or error message.

    char body[1000];
    char endpoint[] = "/dispenser/medication/taken";
    sprintf(body, "{\"fileira\":%d, \"slot\":%d, timeTaken:%lld}", fileira, slot, (long long int)dateTime);

    ESP_LOGI("DISPENSER LID", "CHECK DISPENSER STATE");

    post_req(endpoint, body, res_ignore);

    ESP_LOGI("DISPENSED MEDICATION TAKEN", "------POSTED");
}

void endpoint_dispenser_medication_not_taken(int fileira, int slot, int64_t dateTime)
{
    // Endpoint: POST /dispenser/medication/not-taken
    // Description: The embedded system informs the backend when a pill is not taken within the specified timeframe (e.g., 30 minutes) and is collected.
    // Request Body:
    // slotId: Integer
    // timeOfDispense: DateTime
    // Response: Success or error message.
    // Note: if a pill was dispensed 1H30 and other 2H the system will discard both pills or the time to dispense will be expanded?
    // User Account

    char data[1000];
    char endpoint[] = "/dispenser/medication/not-taken";
    sprintf(data, "{\"fileira\":%d, \"slot\":%d, timeTaken:%lld}", fileira, slot, (long long int)dateTime);

    ESP_LOGI("DISPENSER LID", "CHECK DISPENSER STATE");

    post_req(endpoint, data, res_ignore);

    ESP_LOGI("DISPENSED MEDICATION NOT TAKED", "------POSTED");
}

// static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
// {
//     ((char *)userp)[0] = '\0'; // Ensure string is null-terminated
//     strncat(userp, contents, size * nmemb);
//     return size * nmemb;
// }
