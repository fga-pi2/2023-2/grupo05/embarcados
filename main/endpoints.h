#ifndef ENDPOINTS_H
#define ENDPOINTS_H

#include "cJSON.h"

struct ReqInfo
{
    char *endpoint;
    char *method;
    char *timestamp;
    cJSON *args;
};
typedef struct ReqInfo ReqInfo;

ReqInfo *make_long_poll_call(int *n_req);

int send_req();

void endpoint_dispenser_dispense();

void endpoint_dispenser_lid();

void endpoint_dispenser_slots_id();

void endpoint_dispenser_alerts_discrepancy();

void endpoint_dispenser_alerts_list();

void endpoint_dispenser_medication_taken();

void endpoint_dispenser_medication_not_taken();

#endif
