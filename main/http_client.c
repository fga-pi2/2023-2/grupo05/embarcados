#include <string.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_http_client.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_log.h"

#include "http_client.h"

#define TAG "HTTP"

char last_res[10000];

esp_err_t _http_event_handle(esp_http_client_event_t *evt)
{
    switch (evt->event_id)
    {
    case HTTP_EVENT_ERROR:
        ESP_LOGI(TAG, "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGI(TAG, "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGI(TAG, "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        ESP_LOGI(TAG, "HTTP_EVENT_ON_HEADER");
        printf("%.*s", evt->data_len, (char *)evt->data);
        break;
    case HTTP_EVENT_ON_DATA:
        ESP_LOGI(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        if (!esp_http_client_is_chunked_response(evt->client))
        {
            printf("%.*s", evt->data_len, (char *)evt->data);
        }
        sprintf(last_res, "%.*s", evt->data_len, (char *)evt->data);

        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGI(TAG, "HTTP_EVENT_ON_FINISH");
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
        break;
    case HTTP_EVENT_REDIRECT:
        ESP_LOGI(TAG, "HTTP_EVENT_REDIRECT");
        break;
    }
    return ESP_OK;
}

char *http_get_request(char *url, char *res_buff, int res_size)
{
    esp_http_client_config_t config = {
        .url = url,
        .event_handler = _http_event_handle,
        .auth_type = HTTP_AUTH_TYPE_BASIC,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);

    if (err == ESP_OK)
    {
        ESP_LOGI("HTTP", "HTTP GET Status = %d, content_length = %lld",
                 esp_http_client_get_status_code(client),
                 esp_http_client_get_content_length(client));

        int bytesRead = esp_http_client_read(client, res_buff, sizeof(res_buff));
        ESP_LOGE("HTTP", "GET BYTES READ = %d", bytesRead);
        if (bytesRead == 0)
        {
            ESP_LOGE("HTTP", "HTTP GET request READ failed: %s", esp_err_to_name(err));
            // Handle the error
        }
    }
    else
    {
        ESP_LOGE("HTTP", "HTTP GET request failed: %s", esp_err_to_name(err));
    }

    esp_http_client_cleanup(client);
    return res_buff;
}

char *http_post_request(char *url, char *data, char *res_buff1, int res_size)
{
    ESP_LOGI("http client", "http_post_request");
    esp_http_client_config_t config = {
        .url = url,
        .event_handler = _http_event_handle,
        .auth_type = HTTP_AUTH_TYPE_BASIC,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);

    esp_http_client_set_url(client, url);
    esp_http_client_set_method(client, HTTP_METHOD_POST);
    esp_http_client_set_header(client, "Content-Type", "application/json");
    esp_http_client_set_post_field(client, data, strlen(data));

    ESP_LOGI("http client", "data = %s", data);

    esp_err_t err = esp_http_client_perform(client);

    // [TODO] remove 2 linhas de teste
    res_size = 1000;
    char res_buff[res_size];

    if (err == ESP_OK)
    {
        ESP_LOGI("HTTP", "HTTP POST Status = %d, content_length = %lld",
                 esp_http_client_get_status_code(client),
                 esp_http_client_get_content_length(client));

        int bytesRead = esp_http_client_read(client, res_buff, res_size);
        ESP_LOGI("HTTP", "POST BYTES READ = %d", bytesRead);
        ESP_LOGI("HTTP", "POST LAST RES = %s", last_res);
        sprintf(res_buff1, "%s", last_res);

        if (bytesRead == 0)
        {
            ESP_LOGE("HTTP", "HTTP POST request READ failed: %s", esp_err_to_name(err));
            // Handle the error
        }
    }
    else
    {
        ESP_LOGE("HTTP", "HTTP POST request failed: %s", esp_err_to_name(err));
    }

    esp_http_client_cleanup(client);
    return res_buff;
}

void https_request(char *url)
{
    esp_http_client_config_t config = {
        .url = url,
        .event_handler = _http_event_handle,
        //.cert_pem = howsmyssl_com_root_cert_pem_start,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);

    if (err == ESP_OK)
    {
        // ESP_LOGI(TAG, "Status = %d, content_length = %d",
        //         esp_http_client_get_status_code(client),
        //         esp_http_client_get_content_length(client));
    }
    esp_http_client_cleanup(client);
}
