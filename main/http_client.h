#ifndef HTTP_CLIENT_H
#define HTTP_CLIENT_H

#include <inttypes.h>

char *http_get_request(char *url, char *res_buff, int res_size);
char *http_post_request(char *url, char *data, char *res_buff, int res_size);
// int http_get_request_test(char *url);
// void https_request();

#endif
