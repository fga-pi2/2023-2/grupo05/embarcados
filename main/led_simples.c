#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "led_simples.h"

static const char *TAG = "led_simples";

void init_dispositivo_led_simples(int pin)
{
    ESP_LOGD(TAG, "configurando led simples na GPIO %d", pin);
    gpio_reset_pin(pin);
    gpio_set_direction(pin, GPIO_MODE_OUTPUT);
}

void led_task(void *pvParameter)
{
    int pin = (int)pvParameter;
    gpio_set_direction(pin, GPIO_MODE_OUTPUT);
    gpio_set_level(pin, 1);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    gpio_set_level(pin, 0);
    vTaskDelete(NULL);
}

void set_led(int pin)
{
    ESP_LOGI(TAG, "set_led pin=%d", pin);
    xTaskCreate(&led_task, "led_task", 2048, (void *)pin, 5, NULL);
}
