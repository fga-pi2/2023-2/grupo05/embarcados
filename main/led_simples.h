#ifndef LED_SIMPLES_H
#define LED_SIMPLES_H

void init_dispositivo_led_simples(int pin);

void set_led_simples(int pin, int valor);

#endif