#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "esp_sntp.h"
#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "rtc_wdt.h"
#include "esp_task_wdt.h"
#include <rom/ets_sys.h>

// projeto
#include "controle.h"
#include "wifi.h"
#include "http_client.h"
#include "botao.h"
#include "servo_tower_pro_sg90.h"

#define TAG "MAIN"

#define HTTP_BACKEND "http://15.229.75.24:80"
#define WIFI_START_WAIT_MS 5000

SemaphoreHandle_t conexaoWifiSemaphore;

void thread_controle_dispositivos(void *params)
{
  // ESP_LOGI(TAG, "esperando semaforo do wifi...");
  // // xSemaphoreTake(conexaoWifiSemaphore, portMAX_DELAY);

  // ESP_LOGI(TAG, "semaforo wifi liberado!");

  // ESP_LOGI(TAG, "chamando controlar_sistema_embarcado");
  controlar_sistema_embarcado();

  // int pino_servo = 22;
  // // int pino_botao = 13;

  // init_servo(pino_servo);
  // ligar_servo(pino_servo);
  // // init_botao(pino_botao);

  // // int state = 0;
  // // int last_btn = 0;

  // while (1)
  // {
  // //   if (is_botao_apertado(pino_botao))
  // //   {
  // //     if (last_btn != 1)
  // //     {
  // //       state = 1;
  // //       ESP_LOGI(TAG, "----------- !!!!!!! botao apertado");
  // //     }
  // //     last_btn = 1;
  // //   }
  // //   else
  // //   {
  // //     ESP_LOGI(TAG, "----------- botao nao apertado");
  // //     last_btn = 0;
  // //   }

  // //   if (state)
  // //   {
  // //   }
  // //   else
  // //   {
  // //   }
  //     ESP_LOGI("main", "fechando portinhola");
  //     definir_direcao_servo(pino_servo, 0.0);
  //   vTaskDelay(1000 / portTICK_PERIOD_MS);

  //     ESP_LOGI("main", "abrindo portinhola");
  //     definir_direcao_servo(pino_servo, 45.0);

  //   vTaskDelay(1000 / portTICK_PERIOD_MS);
  // }
}

void connect_wifi(void *params)
{
  wifi_start();
  while (1)
  {
    xSemaphoreGive(conexaoWifiSemaphore);
    vTaskDelay(2000 / portTICK_PERIOD_MS);
  }
}

void app_main(void)
{
  conexaoWifiSemaphore = xSemaphoreCreateBinary();

  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
  {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }

  ESP_ERROR_CHECK(ret);

  ESP_LOGI("MAIN", "Iniciando threads");

  ESP_LOGI("MAIN", "Iniciando thread_controle_dispositivos");
  xTaskCreate(&thread_controle_dispositivos, "thread_controle_dispositivos", 100000, NULL, 1, NULL);

  // ESP_LOGI("MAIN", "Iniciando task_connect_wifi");
  xTaskCreate(&connect_wifi, "task_connect_wifi", 10000, NULL, 1, NULL);
}
