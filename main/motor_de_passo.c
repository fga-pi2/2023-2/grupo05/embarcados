
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "esp_sntp.h"
#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "rtc_wdt.h"
#include "esp_task_wdt.h"
#include <rom/ets_sys.h>

#include "motor_de_passo.h"

// // #include <Stepper.h> //INCLUSÃO DE BIBLIOTECA

// const int stepsPerRevolution = 2048; // NÚMERO DE PASSOS POR VOLTA
// int angulo = 0;
// int x = 1;
// Stepper myStepper(stepsPerRevolution, 8, 10, 9, 11); // INICIALIZA O MOTOR UTILIZANDO OS PINOS DIGITAIS 8, 9, 10, 11

// void init_motor_de_passo()
// {
//   // myStepper.setSpeed(5); //VELOCIDADE DO MOTOR
// }

// void set_angulo_motor_de_passo(int pin, float angulo)
// {
//   angulo = (x * 5.65);
//   myStepper.step(angulo);
//   delay(10000);
// }

MotorDePasso motores[50];

void init_motor_gpio(int pin1, int pin2, int pin3, int pin4)
{
  motores[pin1].pin1 = pin1;
  motores[pin1].pin2 = pin2;
  motores[pin1].pin3 = pin3;
  motores[pin1].pin4 = pin4;
  motores[pin1].steps_per_revolution = 2048;

  gpio_reset_pin(pin1);
  gpio_set_direction(pin1, GPIO_MODE_OUTPUT);
  gpio_reset_pin(pin2);
  gpio_set_direction(pin2, GPIO_MODE_OUTPUT);
  gpio_reset_pin(pin3);
  gpio_set_direction(pin3, GPIO_MODE_OUTPUT);
  gpio_reset_pin(pin4);
  gpio_set_direction(pin4, GPIO_MODE_OUTPUT);
  ESP_LOGE("motor", "liguei");

  // //?? gpio_pad_select_gpio(MOTOR_PIN1);
  // gpio_set_direction(pin1, GPIO_MODE_OUTPUT);
  // gpio_set_direction(pin2, GPIO_MODE_OUTPUT);
  // gpio_set_direction(pin3, GPIO_MODE_OUTPUT);
  // gpio_set_direction(pin4, GPIO_MODE_OUTPUT);
}

void step_motor(int pin1, int step)
{
  MotorDePasso *motor = &motores[pin1];
  // This is a full-step sequence
  switch (step % 4)
  {
  case 0:
    // Step 1: Coil 1 and 2 are activated
    gpio_set_level(motor->pin1, 1);
    gpio_set_level(motor->pin2, 0);
    gpio_set_level(motor->pin3, 1);
    gpio_set_level(motor->pin4, 0);
    break;
  case 1:
    // Step 2: Coil 2 and 3 are activated
    gpio_set_level(motor->pin1, 0);
    gpio_set_level(motor->pin2, 1);
    gpio_set_level(motor->pin3, 1);
    gpio_set_level(motor->pin4, 0);
    break;
  case 2:
    // Step 3: Coil 3 and 4 are activated
    gpio_set_level(motor->pin1, 0);
    gpio_set_level(motor->pin2, 1);
    gpio_set_level(motor->pin3, 0);
    gpio_set_level(motor->pin4, 1);
    break;
  case 3:
    // Step 4: Coil 4 and 1 are activated
    gpio_set_level(motor->pin1, 1);
    gpio_set_level(motor->pin2, 0);
    gpio_set_level(motor->pin3, 0);
    gpio_set_level(motor->pin4, 1);
    break;
  }
}

void rotate_motor(int pin1, float angle)
{
  int steps = (angle / 360.0) * motores[pin1].steps_per_revolution;
  // speed = 5.65
  int delay_time = 10;

  for (int i = 0; i < steps; i++)
  {
    step_motor(pin1, i);
    vTaskDelay(delay_time / portTICK_PERIOD_MS);
  }
}
