#ifndef MOTOR_DE_PASSO_H
#define MOTOR_DE_PASSO_H

struct MotorDePasso
{
  int pin1;
  int pin2;
  int pin3;
  int pin4;
  int steps_per_revolution;
};
typedef struct MotorDePasso MotorDePasso;

void init_motor_gpio(int pin1, int pin2, int pin3, int pin4);

void rotate_motor(int pin1, float angle);

#endif
