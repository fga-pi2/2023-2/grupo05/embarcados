#ifndef MQTT_H
#define MQTT_H

void mqtt_start();
int get_botao_ligado();
void set_botao_ligado(int valor);
void mqtt_envia_mensagem(char * topico, char * mensagem);

#endif