#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "rtc_wdt.h"
#include "esp_task_wdt.h"
#include <rom/ets_sys.h>

#include "driver/gpio.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"

#include "servo_tower_pro_sg90.h"

static const char *TAG = "SERVO";

#define ESP_32_PIN_COUNT 40

int pin_pointer[ESP_32_PIN_COUNT];
int ligado[ESP_32_PIN_COUNT];
float angulo[ESP_32_PIN_COUNT];
int config = 0;

// DATASHEET!!
// http://www.ee.ic.ac.uk/pcheung/teaching/DE1_EE/stores/sg90_datasheet.pdf

void loop_servo(void *p_pin)
{
    int pin = *((int *)p_pin);
    // ESP_LOGI("SERVO LOOP", "rodando o loop do servo (pin %d)\n", pin);
    int i = 0;
    while (1)
    {
        i++;
        if (!ligado[pin])
        {
            // if (i % 100 == 0)
            //     ESP_LOGI("SERVO LOOP", "servo desligado(pin %d)\n", pin);
            gpio_set_level(pin, 0);
            ets_delay_us(20000);
            continue;
        }

        // consulte o datasheet pra entender
        float tempo_ligado = 1000.0 + 1000.0 * (angulo[pin] + 45.0) / 90.0;
        float tempo_desligado = 20000.0 - tempo_ligado;

        // if (i % 100 == 0)
        // {

        //     ESP_LOGI("SERVO LOOP", "SERVO no pino %d | angulo: %.2f | graus tempo_ligado: %d μs | tempo_desligado: %d μs\n", pin, angulo[pin], (int)tempo_ligado, (int)tempo_desligado);
        //     ESP_LOGI("SERVO LOOP", "                                | graus tempo_ligado: %f μs | tempo_desligado: %f μs\n", tempo_ligado, tempo_desligado);
        // }

        gpio_set_level(pin, 1);
        // ESP_LOGI("SERVO LOOP", "                                | graus tempo_ligado: %f μs | tempo_desligado: %f μs\n", tempo_ligado, tempo_desligado);
        ets_delay_us((int)tempo_ligado);

        gpio_set_level(pin, 0);
        // ESP_LOGI("SERVO LOOP", "                                | graus tempo_ligado: %f μs | tempo_desligado: %f μs\n", tempo_ligado, tempo_desligado);
        ets_delay_us((int)tempo_desligado);
    }
}

void init_servo(int pin)
{
    ESP_LOGI(TAG, "init_servo(%d) 1", pin);
    if (!config)
    {

        for (int i = 0; i < ESP_32_PIN_COUNT; i++)
        {
            ligado[i] = 0;
            angulo[i] = 0.0;
            pin_pointer[pin] = -1;
        }
        config = 1;
        ESP_LOGI(TAG, "init_servo(%d) 2", pin);
    }

    ESP_LOGI(TAG, "configurando servo SG90 na GPIO %d", pin);
    gpio_reset_pin(pin);
    gpio_set_direction(pin, GPIO_MODE_OUTPUT);

    pin_pointer[pin] = pin;

    xTaskCreate(&loop_servo, "task_mensageria_servo", 4096, &pin_pointer[pin], 1, NULL);
}

void ligar_servo(int pin)
{
    ligado[pin] = 1;
}

void desligar_servo(int pin)
{
    ligado[pin] = 0;
}

void definir_direcao_servo(int pin, float angulo_em_graus)
{
    // ESP_LOGE("servo definir_direcao_servo()", "pin=%d angulo_antigo=%f angulo_new=%f", pin, angulo[pin], angulo_em_graus);
    angulo[pin] = angulo_em_graus;
}