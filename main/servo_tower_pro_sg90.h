#ifndef SERVO_TOWER_PRO_SG90_H
#define SERVO_TOWER_PRO_SG90_H

void init_servo(int pin);

void ligar_servo(int pin);

void desligar_servo(int pin);

void definir_direcao_servo(int pin, float angulo_em_graus);

#endif