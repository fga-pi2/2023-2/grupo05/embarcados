#ifndef REAL_TIMESTAMP_H
#define REAL_TIMESTAMP_H

#include <inttypes.h>

int64_t get_timestamp();
int64_t get_real_timetamp();

#endif
