from flask import Flask, request, jsonify
import json
import os
import random
import time
import datetime


# Mock data for testing
available_slots = [1, 2, 3, 4, 5]  # Example slot IDs


def get_time_string():
    return datetime.datetime.now().strftime("%Y-%m-%d@%H-%M-%S.%f")


def get_new_messages(timeout=3, chance=0.9):
    res = []
    possibles = [
        {
            "method": "POST",
            "endpoint": "/dispenser/alerts/discrepancy",
            "body": {"slotId": 1, "expected": "occupied", "actual": "empty"},
        },
        {
            "method": "POST",
            "endpoint": "/dispenser/alerts/list",
            "body": [
                {
                    "slot": 1,
                    "fileira": 1,
                }
            ],
        },
    ]
    newt = 0
    while newt < timeout - chance:
        time.sleep(chance)
        newt += chance
        if random.randint(1, 2) == 1:
            continue
        req = random.choice(possibles)
        req["timestamp"] = get_time_string()
        res.append(req)
    return res


def create_app():
    app = Flask(__name__)
    app.secret_key = os.getenv("SECRET_KEY", "secret string")

    @app.route("/", methods=["GET"])
    def healthcheck():
        print("GOT HEALTH CHECK")
        return jsonify({}), 200

    @app.route("/longpoll", methods=["POST"])
    def longpoll():
        print("request:\n", json.dumps(request.json, indent=4))
        resp = {"result": [{"slotId": 1}]}
        new_msgs = get_new_messages()
        return jsonify(new_msgs), 200

    @app.route("/dispenser/dispense", methods=["POST"])
    def dispense_pill():
        # Extract slotId from query parameters
        slotId = request.args.get("slotId", type=int)

        # Check if slotId is provided and valid
        if slotId is None:
            return jsonify({"error": "No slotId provided"}), 400
        elif slotId not in available_slots:
            return jsonify({"error": "Invalid slotId"}), 404

        # Simulate dispensing pill
        # In a real scenario, this would communicate with the embedded system
        print(f"Dispensing pill from slot {slotId}")

        # Return success response
        return (
            jsonify({"result": [{"slotId": slotId, "posteddata": request.json}]}),
            200,
        )

    return app


if __name__ == "__main__":
    from waitress import serve

    app = create_app()
    serve(app, host="0.0.0.0", port=80)


# if __name__ == "__main__":
#     create_app().run(port=3000)
