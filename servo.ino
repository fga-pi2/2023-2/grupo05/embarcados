#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>
#include <Stepper.h>
#include <ESP32Servo.h>

Servo myservo[4];

void setup()
{
    Serial.begin(115200);
    myservo[0].attach(33);
    myservo[4].attach(26);
}

void loop()
{
    //    Serial.print("Abrindo servo 1\n");
    //    myservo[0].write(0);
    //    delay(3000);
    //
    //    Serial.print("Fechando servo 1\n");
    //    myservo[0].write(70);
    //    delay(3000);

    // Sweep from 0 to 180 degrees:

    Serial.print("Servo 5\n");

    for (int angle = 0; angle <= 50; angle += 1)
    {
        myservo[4].write(angle);
        Serial.println(angle);
        delay(15);
    }

    // And back from 180 to 0 degrees:
    for (int angle = 50; angle >= 0; angle -= 1)
    {
        myservo[4].write(angle);
        Serial.println(angle);
        delay(15);
    }
    delay(1000);

    Serial.print("Servo 1\n");

    for (int angle = 0; angle <= 50; angle += 1)
    {
        myservo[0].write(angle);
        Serial.println(angle);
        delay(15);
    }

    // And back from 180 to 0 degrees:
    for (int angle = 50; angle >= 0; angle -= 1)
    {
        myservo[0].write(angle);
        Serial.println(angle);
        delay(15);
    }

    //
    //    Serial.print("Abrindo servo 5\n");
    //    myservo[4].write(10);
    //    delay(3000);
    //
    //    Serial.print("Fechando servo 5\n");
    //    myservo[4].write(105);
    //    delay(3000);
}
